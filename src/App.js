import Cars from "./component/Cars";
import Hello from "./component/Hello";

function App() {
  return (
    <div>
      <Hello />
      <br />
      <Cars />
    </div>
  );
}

export default App;
