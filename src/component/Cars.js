import React from "react";

export default function Cars() {

    const cars = [
        "Ford",
        "BMW",
        "Audi"
    ]

    return (
        <div>
            <h1>Who lives in my garage?</h1>
            <ul>
                {cars.map((ele, ind) => <li>I am a {ele}</li>)}
            </ul>
        </div>
    )
}