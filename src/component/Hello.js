import React, { useState } from "react";
import './Hello.css';

const Hello = () => {
    const [name, setName] = useState("");

    const onClick = () => {
        const name = document.getElementById("name").value;
        setName(name);
    } 

    return (
        <div className="Hello">
            <form>
                <label htmlFor="Name">Enter your name: {name}</label>
                <br />
                <input id="name" name="name" />
                <input type="button" value="Submit" onClick={onClick} />
            </form>
        </div>
    )
}

export default Hello;